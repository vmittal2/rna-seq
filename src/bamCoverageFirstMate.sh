#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=15:00:00
#PBS -q copperhead

module load samtools

cd $PBS_O_WORKDIR

# S,F defined by qsub-doIt.sh
# S - sample name
# F - file name; should be file.bam
# see: https://deeptools.readthedocs.io/en/latest/content/tools/bamCoverage.html#usage-examples-for-rna-seq
opts="--binSize 1 --samFlagInclude 64 --normalizeUsing CPM --outFileFormat bedgraph"
samtools index $F
bamCoverage $opts -b $F -o $S.bedgraph
bgzip $S.bedgraph
tabix -s 1 -b 2 -e 3 $S.bedgraph.gz
