
DrawGeneExprLines <- function(gene="Solyc08g062960.4", 
                              fpm=NULL,
                              codebook=NULL,
                              groups=NULL, samples=getSampleNames(),
                              main=gene,
                              preReadFpkm=NULL,
                              addLines=T, addPoints=T, addArrows=F,
                              addToPlot=FALSE,
                              whenDE=NULL, FDR=NULL){
  groups=codebook$group
  samples=codebook$sample
  if (is.null(fpm)){
    fpm=getFPM(ave = T,sd = T)
  }
  fpm.group=fpm[gene,grep("ave|sd", names(fpkm))]
  aves=as.matrix(fpkm.group[,grep("ave", names(fpkm.group))])
  colnames(aves) = gsub(".ave", "", colnames(aves))
  sds=as.matrix(fpkm.group[,grep("sd", names(fpkm.group))])
  colnames(sds) = gsub(".sd", "", colnames(sds))
  
  # group level plotting stuff
  xg.time = as.numeric(substr(groups, 2,2))
  cat.treat = substr(groups, 1,1)
  group.c = which(cat.treat=="C")
  group.t = which(cat.treat=="T")
  col.c = getSampleGroupColors("C4")
  col.t = getSampleGroupColors("T4")
  
  # get sample values to add points
  #from ProjectVariables.R, assumes gene names are row names.
  if (is.null(preReadFpkm)){
    fpkm=getRPKM(samples, ave=F, sd=F)
  }else{
    fpkm=getRPKM(samples, ave=F, sd=F, preReadD=preReadFpkm) 
  }
  fpkm.s=as.matrix(fpkm[gene,samples])
  x.true = as.numeric(substr(samples, 2,2))
  x.time=jitter(x.true, .2)
  names(x.time) = samples #helpful in drawing arrows
  col.s = getSampleColors(samples)
  
  if (!addToPlot){
    # make empty plot
    par(las=1, mar=c(3,3,3,1), mgp=c(1.5,0.5,0))
    plot(x=x.time, y=fpkm.s, type="n", ylab="FPKM", xlab="time after treatment (hours)", xaxt="n")
    title(main)
    axis(side=1, at=x.true)
  }
  
  if (addLines){
    # add lines
    points(x=xg.time[group.c], y=aves[group.c], type="l", col=col.c, lwd=3)
    points(x=xg.time[group.t], y=aves[group.t], type="l", col=col.t, lwd=3)
  }
  
  if (addPoints){
    # add individual points
    points(x=x.time, y=fpkm.s, col=col.s, pch=16)
  }
  
  if (addArrows){
    # add arrows to indicate samples from the same flatpair
    # which samples do you have a treament and a control for?
    set = intersect(gsub('C',"",samples), 
                    gsub('T',"",samples))
    setC = paste0('C', set)
    setT = paste0('T', set)
    arrows(x0=x.time[setC],
           x1=x.time[setT],
           y0=fpkm.s[,setC],
           y1=fpkm.s[,setT], 
           col="brown", lwd=2, code=2, length=.2, angle=20)
  }
  
  if (!is.null(whenDE) & !is.null(FDR)){
    # add red * to indicate which time points are DE,
    # and print the FDR used to define DE
    yStars = par("usr")[3]
    points(x=whenDE, y=rep(yStars, length(whenDE)), pch="*", col="red", cex=2, xpd=T)
    text(labels=paste0("FDR=", FDR), x=6, y=yStars, pos=1, col="red", xpd=T)
  }
  
}
