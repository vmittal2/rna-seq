# What's here

This directory contains shell scripts for processing and deploying data. 

It also contains R code (**Common.R**) used in multiple places in the project. 

# Data processing shell scripts

* `qsub-doIt.sh` - general purpose script for submitting jobs on torque cluster (uses `qsub`)
* `bamCoverage.sh` - creates bigwig coverage graph file from BAM files
* `find_junctions.sh` - produces BED files with score junctions inferred from RNA-Seq data; uses [Loraine Lab Find Junctions](https://bitbucket.org/lorainelab/findjunctions/src/master/)
* `hisat2-build.sh` - builds index for spliced alignment tool `hisat2`
* `hisat2.sh` - runs spliced alignment tool `hisat2`
* `samtools.sh` - sort and index SAM files; makes BAM files

# Data deployment python script(s)

* `makeAnnotsXml.py` - generates annots.xml file for IGB Quickload (see http://lorainelab-quickload.scidas.org/rnaseq/S_lycopersicum_Sep_2019/annots.xml)
